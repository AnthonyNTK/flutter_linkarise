import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_linkarise/styles/theme.dart';
import 'package:flutter_linkarise/blocs/themes/themes.dart';


class SettingScreen extends StatefulWidget {
  final Function toggleTheme;

  SettingScreen({this.toggleTheme});

  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  String themeToString(ThemeData themeData) {
    return themeData == THEME[AppTheme.Dark] ? 'Dark Mode' : 'Light Mode';
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ThemesBloc, ThemesState>(
      builder: (BuildContext context, ThemesState state) {
        return Container(
          color: Colors.blue,
          child: Center(
            child: RaisedButton.icon(
              icon: Icon(Icons.invert_colors),
              label: Text(themeToString(state.props.first)),
              onPressed: widget.toggleTheme,
            ),
          ),
        );
      },
    );
  }
}
