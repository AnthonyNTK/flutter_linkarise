import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_linkarise/blocs/blocs.dart';
import 'package:flutter_linkarise/models/link.dart';
import 'package:flutter_linkarise/utils/constant.dart';
import 'package:flutter_linkarise/screens/widgets/widgets.dart';

class HomeScreen extends StatefulWidget {
  final Function initData;

  HomeScreen({this.initData});

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<Link> links = List();

  @override
  void initState() {
    super.initState();
    widget.initData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void toggleSnackbar(String message) {
    final snackBar =
        SnackBar(content: Text(message, style: TextStyle(color: Colors.grey)));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LinksBloc, LinksState>(
        condition: (previousState, currentState) =>
            previousState.runtimeType != currentState.runtimeType,
        builder: (BuildContext context, LinksState state) {
          if (state is LinksLoaded) links = state.props;
          if (state is LinksLoadNotAvailable)
            WidgetsBinding.instance
                .addPostFrameCallback((_) => toggleSnackbar(state.message));

          return Stack(
            children: <Widget>[
              ListView.builder(
                  itemCount: links.length,
                  itemBuilder: (BuildContext c, int index) {
                    return PreviewCard(
                        link: links[index], isLast: index == links.length - 1);
                  }),
              state is LinksLoading ? LoadingIndicator() : Container(),
              SearchSheet(
                blocType: BLOC_TYPE.LINK,
                placeholder: 'https://www.exmaple.com',
                initialSize: 0.3,
              )
            ],
          );
        });
  }
}
