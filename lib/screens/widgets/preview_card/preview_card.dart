import 'package:flutter/material.dart';

import 'package:flutter_linkarise/models/link.dart';
import 'package:flutter_linkarise/screens/detail_screen.dart';
import 'package:flutter_linkarise/screens/widgets/preview_card/content.dart';
import 'package:flutter_linkarise/screens/widgets/preview_card/card_button.dart';

class PreviewCard extends StatelessWidget {
  final Link link;

  final bool isLast;

  PreviewCard({this.link, this.isLast});

  bool isLongDescription() {
    return link.description.length > 130;
  }

  bool isSuperLongDescription() {
    return link.description.length > 180;
  }

  Widget bannerCard(
      {bool portrait,
      Color color,
      BannerLocation location,
      String message,
      Widget nestedChild}) {
    return Banner(
        message: message,
        color: color,
        location: location,
        child: nestedChild == null ? cardContent(portrait) : nestedChild);
  }

  Widget cardContent(bool portrait) {
    return LinkContent(
        link: link, portrait: portrait, isLongDescription: isLongDescription());
  }

  @override
  Widget build(BuildContext context) {
    bool portrait = MediaQuery.of(context).orientation == Orientation.portrait;
    return GestureDetector(
      onTap: () => Navigator.push(context, MaterialPageRoute(builder: (_) {
        return DetailScreen(link);
      })),
      child: Container(
          height: portrait
              ? isLongDescription() ? 250 : 156
              : isSuperLongDescription() ? 250 : 200,
          margin: EdgeInsets.fromLTRB(10, 10, 10, isLast ? 120 : 10),
          decoration: BoxDecoration(
              border: Border.all(width: 1, color: Colors.grey),
              borderRadius: BorderRadius.circular(8)),
          child: portrait
              ? Column(
                  children: <Widget>[
                    Expanded(
                        flex: isLongDescription() ? 9 : 4,
                        child: Container(
                            child: link.isRecent
                                ? ClipRect(
                                    child: bannerCard(
                                        portrait: portrait,
                                        color: Colors.red,
                                        location: BannerLocation.topEnd,
                                        message: "Latest",
                                        nestedChild: link.tags.length == 0
                                            ? bannerCard(
                                                portrait: portrait,
                                                color: Colors.yellow,
                                                location:
                                                    BannerLocation.topStart,
                                                message: "No Tag",
                                              )
                                            : null))
                                : link.tags.length == 0
                                    ? bannerCard(
                                        portrait: portrait,
                                        color: Colors.yellow,
                                        location: BannerLocation.topStart,
                                        message: "No Tag",
                                      )
                                    : cardContent(portrait))),
                    Expanded(
                        flex: 1,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                CardButton(
                                    icon: Icons.edit,
                                    title: 'Tag',
                                    size: 18,
                                    width: 46,
                                    callback: () {}),
                                CardButton(
                                    icon: Icons.star_border,
                                    title: 'Like',
                                    size: 18,
                                    width: 48,
                                    callback: () {}),
                              ],
                            ),
                            CardButton(
                                icon: Icons.share,
                                title: 'Share',
                                size: 18,
                                width: 65,
                                callback: () {})
                          ],
                        ))
                  ],
                )
              : Row(
                  children: <Widget>[
                    Expanded(
                        flex: 4,
                        child: Container(
                            decoration: BoxDecoration(
                                border: Border(
                                    right: BorderSide(
                                        color: Colors.grey, width: 1))),
                            child: LinkContent(
                                link: link,
                                portrait: portrait,
                                isLongDescription: isLongDescription()))),
                    Expanded(
                        flex: 1,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            CardButton(
                                icon: Icons.edit,
                                title: 'Edit',
                                size: 20,
                                width: 80,
                                callback: () {}),
                            CardButton(
                                icon: Icons.star_border,
                                title: 'Like',
                                size: 20,
                                width: 80,
                                callback: () {}),
                            CardButton(
                                icon: Icons.share,
                                title: 'Share',
                                size: 20,
                                width: 80,
                                callback: () {}),
                            CardButton(
                                icon: Icons.delete,
                                title: 'Delete',
                                size: 20,
                                width: 80,
                                callback: () {})
                          ],
                        ))
                  ],
                )),
    );
  }
}
