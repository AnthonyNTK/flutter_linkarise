import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_linkarise/blocs/blocs.dart';
import 'package:flutter_linkarise/models/link.dart';

class TagList extends StatefulWidget {
  final Link link;
  final List<String> tags;
  final int displayNum;

  TagList({this.link, this.tags, this.displayNum});

  @override
  _TagListState createState() => _TagListState();
}

class _TagListState extends State<TagList> {
  List<String> tags;
  List<String> newTags = List();
  int displayNum;
  bool isDuplicate = false;

  @override
  void initState() {
    super.initState();
    tags = widget.tags;
    displayNum = widget.displayNum;
  }

  @override
  void dispose() {
    super.dispose();
  }

  updateTagList() {
    tags.addAll(newTags);
    displayNum += newTags.length;
    newTags.clear();

    Link updatedLink = widget.link.copyWith(tags: tags);
    BlocProvider.of<LinksBloc>(context).add(UpdateLink(updatedLink));
    Navigator.pop(context, 'done');
  }

  Future<String> showMessageDialog(BuildContext scaffoldContext) {
    TextEditingController textEditingController = TextEditingController();

    return showDialog(
        context: scaffoldContext,
        barrierDismissible: false,
        child: StatefulBuilder(builder: (context, setState) {
          return AlertDialog(
              title: Text('Add Tags'),
              content: Container(
                height: 150,
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: SingleChildScrollView(
                        child: Wrap(
                          children: newTags
                              .map((String tag) => Chip(
                                  label: Text(tag),
                                  onDeleted: () =>
                                      setState(() => newTags.remove(tag))))
                              .toList(),
                        ),
                      ),
                    ),
                    isDuplicate
                        ? Container(
                            child: Text(
                              'Tag already exist!',
                              style: TextStyle(color: Colors.redAccent),
                            ),
                          )
                        : Container(),
                    TextField(
                      controller: textEditingController,
                      decoration: InputDecoration(
                          suffixIcon: IconButton(
                            icon: Icon(Icons.check),
                            onPressed: () {
                              String tag = textEditingController.text;
                              setState(() {
                                if (newTags.contains(tag) ||
                                    tags.contains(tag)) {
                                  isDuplicate = true;
                                } else {
                                  isDuplicate = false;
                                  newTags.add(tag);
                                }
                              });
                              WidgetsBinding.instance.addPostFrameCallback(
                                  (_) => textEditingController.clear());
                            },
                          ),
                          border: OutlineInputBorder(),
                          labelText: 'Tag'),
                    )
                  ],
                ),
              ),
              actions: <Widget>[
                FlatButton(
                  onPressed: () {
                    Navigator.pop(context, '');
                  },
                  child: Text('CANCEL'),
                ),
                FlatButton(
                  onPressed: updateTagList,
                  child: Text('SAVE'),
                ),
              ]);
        }));
  }

  List<Widget> buildTagList(BuildContext context) {
    List<Widget> widgets = List();

    tags.sublist(0, displayNum).forEach((tag) => widgets.add(Chip(
          label: Text(tag),
        )));

    widgets.add(IconButton(
      padding: EdgeInsets.symmetric(horizontal: 5),
      icon: Icon(Icons.add),
      iconSize: 22,
      onPressed: () => showMessageDialog(context).then((value) {
        if (value != '') setState(() {});
      }),
    ));

    return widgets;
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(children: buildTagList(context));
  }
}
