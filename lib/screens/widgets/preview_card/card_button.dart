import 'package:flutter/material.dart';

class CardButton extends StatelessWidget {
  final IconData icon;

  final Function callback;

  final String title;
  final double size;
  final double width;

  CardButton({this.icon, this.title, this.size, this.width, this.callback});

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        borderRadius: BorderRadius.circular(15),
        onTap: callback,
        child: Container(
          width: width,
          margin: EdgeInsets.symmetric(horizontal: 8, vertical: 2),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(flex: 1, child: Icon(icon, size: size)),
              title == null
                  ? Container()
                  : Expanded(
                  flex: 2,
                  child: Container(
                      padding: EdgeInsets.only(left: 2),
                      child: Text(title,
                          style: TextStyle(fontSize: size - 2))))
            ],
          ),
        ),
      ),
    );
  }
}
