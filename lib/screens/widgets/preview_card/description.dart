import 'package:flutter/material.dart';

class LinkDescription extends StatelessWidget {
  final String description;

  LinkDescription(this.description);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Text(description, style: TextStyle(color: Colors.grey)));
  }
}
