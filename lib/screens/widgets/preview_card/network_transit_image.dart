import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';

class NetworkTransitImage extends StatelessWidget {
  final String url;

  NetworkTransitImage(this.url);

  @override
  Widget build(BuildContext context) {
    return FadeInImage.memoryNetwork(
        fit: BoxFit.fitWidth, placeholder: kTransparentImage, image: url);
  }
}
