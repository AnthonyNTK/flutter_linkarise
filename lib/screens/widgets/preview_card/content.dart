import 'package:flutter/material.dart';
import 'package:timeago/timeago.dart' as relativeTime;

import 'package:flutter_linkarise/models/link.dart';
import 'package:flutter_linkarise/screens/widgets/title.dart';
import 'package:flutter_linkarise/screens/widgets/preview_card/description.dart';
import 'package:flutter_linkarise/screens/widgets/preview_card/network_transit_image.dart';

class LinkContent extends StatelessWidget {
  final Link link;
  final bool portrait;
  final bool isLongDescription;

  LinkContent({this.link, this.portrait, this.isLongDescription});

  String trimDescription() {
    final int maxLength = isLongDescription ? 130 : link.description.length;
    return link.description.substring(0, maxLength) + '...';
  }

  Widget bigCardContent() {
    return Stack(
      children: <Widget>[
        Column(
          children: <Widget>[
            Hero(
              tag: link.id,
              child: Container(
                height: 105,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: ClipRRect(
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(8),
                              topLeft: Radius.circular(8)),
                          child: NetworkTransitImage(link.image)),
                    ),
                  ],
                ),
              ),
            ),
            SectionTitle(link.title),
            LinkDescription(portrait ? trimDescription() : link.description)
          ],
        ),
        Positioned(
            right: 10,
            bottom: 0,
            child: Container(
                height: 24,
                child: Text(relativeTime.format(link.createdAt),
                    textAlign: TextAlign.end)))
      ],
    );
  }

  Widget smallCardContent() {
    return Stack(
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(
              flex: 3,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SectionTitle(link.title),
                  LinkDescription(link.description)
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                padding: EdgeInsets.only(right: 10),
                child:
                    Hero(tag: link.id, child: NetworkTransitImage(link.image)),
              ),
            )
          ],
        ),
        Positioned(
            right: 10,
            bottom: -10,
            child: Container(
                height: 24,
                child: Text(relativeTime.format(link.createdAt),
                    textAlign: TextAlign.end)))
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return portrait
        ? isLongDescription ? bigCardContent() : smallCardContent()
        : bigCardContent();
  }
}
