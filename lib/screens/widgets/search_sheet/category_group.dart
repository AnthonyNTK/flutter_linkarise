import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

import 'package:flutter_linkarise/blocs/blocs.dart';
import 'package:flutter_linkarise/utils/constant.dart';

final List<CategoryButtonItem> categories = [
  CategoryButtonItem(icon: Icon(Icons.category), title: CATEGORY.TAG),
  CategoryButtonItem(icon: Icon(Icons.menu), title: CATEGORY.CONTENT),
  CategoryButtonItem(icon: Icon(Icons.library_books), title: CATEGORY.DATE),
];

class CategoryButtonItem {
  final Icon icon;
  final CATEGORY title;

  CategoryButtonItem({@required this.icon, @required this.title}) {
    assert(icon != null);
  }
}

class CategoryGroup extends StatefulWidget {
  @override
  _CategoryGroupState createState() => _CategoryGroupState();
}

class _CategoryGroupState extends State<CategoryGroup> {
  List<CATEGORY> tags = List();

  void toggleCategoryButton(CATEGORY tag) {
    setState(() {
      if (tags.contains(tag)) {
        tags.remove(tag);
      } else {
        tags.add(tag);
        if (tag == CATEGORY.DATE) {
          DatePicker.showDatePicker(context,
              showTitleActions: true,
              minTime: DateTime(2018, 1, 1),
              maxTime: DateTime(2047, 12, 31), onConfirm: (date) {
            BlocProvider.of<FiltersBloc>(context).add(ApplyFilter(
                payload: date.toIso8601String(),
                key: describeEnum(CATEGORY.DATE).toLowerCase()));
          }, currentTime: DateTime.now(), locale: LocaleType.en);
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 130,
      child: Row(
        children: categories
            .map((item) => Expanded(
                  child: Card(
                    color: tags.contains(item.title)
                        ? Colors.teal
                        : Colors.black54,
                    margin: EdgeInsets.symmetric(horizontal: 4, vertical: 10),
                    child: InkWell(
                      onTap: () => toggleCategoryButton(item.title),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          item.icon,
                          Container(
                              padding: EdgeInsets.only(top: 5),
                              child: Text(describeEnum(item.title)))
                        ],
                      ),
                    ),
                  ),
                ))
            .toList(),
      ),
    );
  }
}
