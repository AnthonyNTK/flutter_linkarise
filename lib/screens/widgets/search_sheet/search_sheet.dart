import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_linkarise/blocs/blocs.dart';
import 'package:flutter_linkarise/utils/constant.dart';

class SearchSheet extends StatefulWidget {
  final BLOC_TYPE blocType;

  final String placeholder;

  final List<Widget> content;

  final double initialSize;

  SearchSheet(
      {this.blocType, this.placeholder, this.content, this.initialSize});

  @override
  _SearchSheetState createState() => _SearchSheetState();
}

class _SearchSheetState extends State<SearchSheet> {
  TextEditingController textEditingController;

  @override
  void initState() {
    super.initState();
    textEditingController = TextEditingController();
  }

  @override
  void dispose() {
    textEditingController.dispose();
    super.dispose();
  }

  void clearInputText() {
    textEditingController.clear();
  }

  void search() {
    FocusScope.of(context).requestFocus(FocusNode());

    if (textEditingController.text != '') {
      switch (widget.blocType) {
        case BLOC_TYPE.FILTER:
          {
            BlocProvider.of<FiltersBloc>(context)
                .add(AddFilter(textEditingController.text));
          }
          break;

        case BLOC_TYPE.LINK:
          {
            BlocProvider.of<LinksBloc>(context)
                .add(AddLink(textEditingController.text));
          }
          break;

        default:
          {
            throw Exception('No such bloc');
          }
      }
      clearInputText();
    }
  }

  List<Widget> content() {
    List<Widget> widgets = List();

    widgets.add(Container(
      decoration: BoxDecoration(
          border: Border.all(
              color: BlocProvider.of<ThemesBloc>(context).state is LightTheme
                  ? Colors.black
                  : Colors.grey,
              width: 1),
          borderRadius: BorderRadius.circular(8)),
      padding: EdgeInsets.symmetric(horizontal: 5),
      child: TextField(
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: widget.placeholder,
          prefixIcon: InkWell(onTap: search, child: Icon(Icons.search)),
          suffixIcon: InkWell(onTap: clearInputText, child: Icon(Icons.cancel)),
        ),
        style: TextStyle(fontSize: 20),
        controller: textEditingController,
      ),
    ));

    if (widget.content != null) widget.content.forEach((w) => widgets.add(w));

    return widgets;
  }

  @override
  Widget build(BuildContext buildContext) {
    return DraggableScrollableSheet(
      initialChildSize: widget.initialSize,
      minChildSize: 0.15,
      maxChildSize: widget.initialSize,
      builder: (BuildContext context, ScrollController scrollController) {
        return ClipRRect(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16), topRight: Radius.circular(16)),
          child: Container(
              padding: EdgeInsets.fromLTRB(20, 25, 20, 20),
              color:
                  BlocProvider.of<ThemesBloc>(buildContext).state is LightTheme
                      ? Colors.grey
                      : Colors.black,
              child: ListView(
                controller: scrollController,
                children: content(),
              )),
        );
      },
    );
  }
}
