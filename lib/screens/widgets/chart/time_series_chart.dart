import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

import 'package:flutter_linkarise/models/time_series_count.dart';

class AxisTheme {
  static charts.RenderSpec<num> axisThemeNum() {
    return charts.GridlineRendererSpec(
      labelStyle: charts.TextStyleSpec(
        color: charts.MaterialPalette.gray.shade500,
      ),
      lineStyle: charts.LineStyleSpec(
        color: charts.MaterialPalette.gray.shade500,
      ),
    );
  }

  static charts.RenderSpec<DateTime> axisThemeDateTime() {
    return charts.GridlineRendererSpec(
      labelStyle: charts.TextStyleSpec(
        color: charts.MaterialPalette.gray.shade500,
      ),
      lineStyle: charts.LineStyleSpec(
        color: charts.MaterialPalette.transparent,
      ),
    );
  }
}

class TimeSeriesChart extends StatelessWidget {
  final List<TimeSeriesCount> seriesList;

  TimeSeriesChart(this.seriesList);

  @override
  Widget build(BuildContext context) {
    charts.RenderSpec<num> renderSpecPrimary = AxisTheme.axisThemeNum();
    charts.RenderSpec<DateTime> renderSpecDomain =
        AxisTheme.axisThemeDateTime();

    return new charts.TimeSeriesChart(
      _createSampleData(),
      animate: true,
      dateTimeFactory: const charts.LocalDateTimeFactory(),
      primaryMeasureAxis: charts.NumericAxisSpec(
        tickProviderSpec: charts.BasicNumericTickProviderSpec(
          zeroBound: false,
        ),
        renderSpec: renderSpecPrimary,
      ),
      domainAxis: charts.DateTimeAxisSpec(
        renderSpec: renderSpecDomain,
      ),
    );
  }

  List<charts.Series<TimeSeriesCount, DateTime>> _createSampleData() {
    return [
      new charts.Series<TimeSeriesCount, DateTime>(
        id: 'Count',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (TimeSeriesCount sales, _) => sales.time,
        measureFn: (TimeSeriesCount sales, _) => sales.count,
        data: seriesList,
      )
    ];
  }
}
