import 'package:flutter/material.dart';
import 'package:flutter_linkarise/screens/widgets/chart/animated_count.dart';

class StaticChart extends StatelessWidget {
  final int totalTag;
  final int totalLink;
  final int maxTag;

  final double averageTag;
  final double averageLink;

  final int totalTitleChar;
  final int totalDescriptionChar;

  final double averageTitleChar;
  final double averageDescriptionChar;

  StaticChart(
      {this.totalLink,
      this.totalTag,
      this.maxTag,
      this.averageLink,
      this.averageTag,
      this.totalDescriptionChar,
      this.totalTitleChar,
      this.averageDescriptionChar,
      this.averageTitleChar});

  Widget cardTitle(String title) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 5),
      child: Text(title),
    );
  }

  Widget statisticDescription(String description, num number) {
    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
              padding: EdgeInsets.only(left: 10), child: Text(description)),
          Container(
              padding: EdgeInsets.only(right: 10),
              child:
                  AnimatedCount(count: number, duration: Duration(seconds: 1)))
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    bool portrait = MediaQuery.of(context).orientation == Orientation.portrait;
    return Container(
      height: 200,
      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Card(
              margin: EdgeInsets.symmetric(horizontal: 5),
              child: Column(
                children: <Widget>[
                  cardTitle('User Statistic'),
                  statisticDescription('Average Tag: ', averageTag),
                  statisticDescription('Total Tag: ', totalTag),
                  statisticDescription('Maximum Tag: ', maxTag),
                  statisticDescription('Average Link: ', averageLink),
                  statisticDescription('Total Link: ', totalLink),
                ],
              ),
            ),
          ),
          Expanded(
            child: Card(
              margin: EdgeInsets.symmetric(horizontal: 5),
              child: Column(
                children: <Widget>[
                  cardTitle('Data Statistic'),
                  statisticDescription(
                      portrait ? 'Avg Title Len: ' : 'Average Title Length: ',
                      averageTitleChar),
                  statisticDescription(
                      portrait ? 'Total Title Len: ' : 'Total Title Length: ',
                      totalTitleChar),
                  statisticDescription(
                      portrait
                          ? 'Avg Desc Len: '
                          : 'Average Description Length: ',
                      averageDescriptionChar),
                  statisticDescription(
                      portrait
                          ? 'Total Desc Len: '
                          : 'Total Description Length: ',
                      totalDescriptionChar),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
