import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:charts_flutter/flutter.dart' as charts;

import 'package:flutter_linkarise/blocs/blocs.dart';
import 'package:flutter_linkarise/models/tag_pie_count.dart';

class TagPieChart extends StatelessWidget {
  final List<TagPieCount> data;

  TagPieChart(this.data);

  @override
  Widget build(BuildContext context) {
    return charts.PieChart(_createSampleData(),
        animate: true,
        defaultRenderer:
            charts.ArcRendererConfig(arcWidth: 60, arcRendererDecorators: [
          charts.ArcLabelDecorator(
              outsideLabelStyleSpec: charts.TextStyleSpec(
            color: BlocProvider.of<ThemesBloc>(context).state is LightTheme
                ? charts.Color.black
                : charts.Color.white,
            fontSize: 12,
          ))
        ]));
  }

  List<charts.Series<TagPieCount, int>> _createSampleData() {
    return [
      charts.Series<TagPieCount, int>(
        id: 'Count',
        domainFn: (TagPieCount tagCount, _) => data.indexOf(tagCount),
        measureFn: (TagPieCount tagCount, _) => tagCount.count,
        data: data,
        labelAccessorFn: (TagPieCount tagCount, _) =>
            '${tagCount.tag}: ${tagCount.count}',
      )
    ];
  }
}
