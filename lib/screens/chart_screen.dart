import 'package:flutter/material.dart';

import 'package:flutter_linkarise/utils/api.dart';
import 'package:flutter_linkarise/models/tag_pie_count.dart';
import 'package:flutter_linkarise/models/time_series_count.dart';
import 'package:flutter_linkarise/screens/widgets/chart/pie_chart.dart';
import 'package:flutter_linkarise/screens/widgets/chart/static_chart.dart';
import 'package:flutter_linkarise/screens/widgets/chart/time_series_chart.dart';

class ChartScreen extends StatefulWidget {
  @override
  _ChartScreenState createState() => _ChartScreenState();
}

class _ChartScreenState extends State<ChartScreen> {
  List<TimeSeriesCount> seriesCounts = List();
  List<TagPieCount> tagCounts = List();

  int totalTag = 0;
  int totalLink = 0;
  int maxTag = 0;

  double averageTag = 0;
  double averageLink = 0;

  int totalTitleChar = 0;
  int totalDescriptionChar = 0;

  double averageTitleChar = 0;
  double averageDescriptionChar = 0;

  @override
  void initState() {
    super.initState();

    API.getUserStatistic().then((data) {
      setState(() {
        List<DateTime> sortedKey = List();

        Map statistic = data;
        List linkKeys = statistic['linkCount'].keys.toList()..sort();
        for (var key in linkKeys) {
          List<String> datePart = key.split('-');
          sortedKey.add(DateTime(int.parse(datePart[0]), int.parse(datePart[1]),
              int.parse(datePart[2])));
        }
        sortedKey.sort((a, b) => a.compareTo(b));

        for (DateTime key in sortedKey) {
          String realKey = '${key.year}-${key.month}-${key.day}';
          seriesCounts.add(TimeSeriesCount(
              time: key, count: statistic['linkCount'][realKey]));
        }

        List tagKeys = statistic['tagCount'].keys.toList();
        for (var key in tagKeys) {
          int tagCount = statistic['tagCount'][key];
          tagCounts.add(TagPieCount(tag: key, count: tagCount));
        }
        tagCounts.sort((a, b) => (b.count.compareTo(a.count)));

        totalTag = statistic['totalTag'];
        totalLink = statistic['totalLink'];
        maxTag = statistic['maxTag'];
        averageTag = statistic['averageTag'];
        averageLink = statistic['averageLink'];
      });
    });

    API.getDataStatistic().then((data) {
      setState(() {
        Map statistic = data;

        totalTitleChar = statistic['totalTitleChar'];
        totalDescriptionChar = statistic['totalDescriptionChar'];
        averageTitleChar = statistic['averageTitleChar'];
        averageDescriptionChar = statistic['averageDescriptionChar'];
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget cardTitle(String title) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 5),
      child: Text(title),
    );
  }

  Widget cardContent({String title, Widget chart}) {
    return Card(
        margin: EdgeInsets.all(10),
        elevation: 5,
        child: Container(
          padding: EdgeInsets.all(5),
          height: 200,
          child: Column(children: <Widget>[
            cardTitle(title),
            Expanded(child: chart),
          ]),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        cardContent(
            title: 'Input Count',
            chart: seriesCounts.length > 0
                ? TimeSeriesChart(seriesCounts)
                : Center(child: CircularProgressIndicator())),
        cardContent(
            title: 'Tag Count',
            chart: tagCounts.length > 0
                ? TagPieChart(tagCounts.sublist(0, 6))
                : Center(child: CircularProgressIndicator())),
        StaticChart(
            averageLink: averageLink,
            averageTag: averageTag,
            maxTag: maxTag,
            totalLink: totalLink,
            totalTag: totalTag,
            averageDescriptionChar: averageDescriptionChar,
            totalDescriptionChar: totalDescriptionChar,
            averageTitleChar: averageTitleChar,
            totalTitleChar: totalTitleChar)
      ],
    );
  }
}
