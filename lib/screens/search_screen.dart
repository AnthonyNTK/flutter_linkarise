import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_linkarise/blocs/blocs.dart';
import 'package:flutter_linkarise/models/link.dart';
import 'package:flutter_linkarise/utils/constant.dart';
import 'package:flutter_linkarise/screens/widgets/widgets.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  List<Link> links = List();

  void toggleSnackbar(String message) {
    final snackBar =
        SnackBar(content: Text(message, style: TextStyle(color: Colors.grey)));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext buildContext) {
    return BlocBuilder<FiltersBloc, FiltersState>(
        condition: (previousState, currentState) =>
            (previousState.runtimeType != currentState.runtimeType),
        builder: (BuildContext context, FiltersState state) {
          if (state is FiltersLoaded) links = state.props;
          if (state is FiltersLoadNotAvailable)
            WidgetsBinding.instance
                .addPostFrameCallback((_) => toggleSnackbar(state.message));

          return Stack(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).size.height / 10 * 1.5),
                child: links.length == 0
                    ? Container(
                        padding: EdgeInsets.only(top: 150),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text('No Result Found', textScaleFactor: 2)
                          ],
                        ))
                    : ListView.builder(
                        itemCount: links.length,
                        itemBuilder: (BuildContext c, int index) {
                          return PreviewCard(
                              link: links[index],
                              isLast: index == links.length - 1);
                        }),
              ),
              state is FiltersLoading ? LoadingIndicator() : Container(),
              BlocBuilder<FiltersBloc, FiltersState>(
                  builder: (BuildContext context, FiltersState state) {
                return SearchSheet(
                  blocType: BLOC_TYPE.FILTER,
                  placeholder: 'tags, title, description',
                  initialSize: 0.4,
                  content: <Widget>[
                    Container(
                        padding: EdgeInsets.only(top: 15),
                        child: SectionTitle('Filters')),
                    CategoryGroup(),
                    Container(
                        padding: EdgeInsets.only(top: 15),
                        child: SectionTitle('Popular Tag')),
                  ],
                );
              })
            ],
          );
        });
  }
}
