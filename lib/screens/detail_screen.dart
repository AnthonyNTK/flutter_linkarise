import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_linkarise/blocs/blocs.dart';
import 'package:flutter_linkarise/models/link.dart';
import 'package:flutter_linkarise/screens/widgets/widgets.dart';
import 'package:flutter_linkarise/screens/widgets/preview_card/description.dart';

class DetailScreen extends StatefulWidget {
  final Link link;

  DetailScreen(this.link);

  @override
  _DetailScreenState createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void removeLink(String value) {
    if (value == 'Delete') {
      BlocProvider.of<LinksBloc>(context).add(DeleteLink(widget.link.id));
      Navigator.pop(context);
    }
  }

  Widget sectionTitle(String title, EdgeInsets margin) {
    return Container(
      padding: EdgeInsets.fromLTRB(0, 10, 0, 5),
      margin: margin,
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.grey, width: 1))),
      height: 40,
      child: Row(
        children: <Widget>[
          Text(title,
              style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Colors.grey))
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Detail'),
        centerTitle: true,
        actions: <Widget>[
          PopupMenuButton<String>(
            onSelected: removeLink,
            itemBuilder: (BuildContext context) {
              return ['Delete'].map((String title) {
                return PopupMenuItem<String>(
                  value: title,
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.delete),
                      Text(title)
                    ],
                  ),
                );
              }).toList();
            },
          ),
        ],
      ),
      body: AnimatedContainer(
        duration: Duration(milliseconds: 200),
        child: SingleChildScrollView(
          padding: EdgeInsets.only(bottom: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Hero(
                tag: widget.link.id,
                child: Container(
                  height: 105,
                  child: Row(
                    children: <Widget>[
                      Expanded(child: NetworkTransitImage(widget.link.image)),
                    ],
                  ),
                ),
              ),
              sectionTitle('Title', EdgeInsets.symmetric(horizontal: 10)),
              SectionTitle(widget.link.title),
              sectionTitle('Description',
                  EdgeInsets.symmetric(horizontal: 10, vertical: 10)),
              LinkDescription(widget.link.description),
              sectionTitle(
                  'Tags', EdgeInsets.symmetric(horizontal: 10)),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  child: TagList(
                    link: widget.link,
                    tags: widget.link.tags,
                    displayNum: widget.link.tags.length,
                  )),
              sectionTitle('MetaData',
                  EdgeInsets.symmetric(horizontal: 10, vertical: 10)),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                  child: Text('Created at: ' +
                      widget.link.createdAt.toIso8601String())),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                  child: Text('URL: ' + widget.link.url)),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                  child: Text('Last Modified at: '))
            ],
          ),
        ),
      ),
    );
  }
}
