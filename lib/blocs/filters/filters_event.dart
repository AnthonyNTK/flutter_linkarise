import 'package:equatable/equatable.dart';

abstract class FiltersEvent extends Equatable {
  const FiltersEvent();

  @override
  List<Object> get props => [];
}

class ApplyFilter extends FiltersEvent {
  final String key;

  final Object payload;

  const ApplyFilter({this.payload, this.key});

  @override
  String toString() => 'ApplyFilter { filter: $payload }';
}

class AddFilter extends FiltersEvent {
  final String query;

  const AddFilter(this.query);

  @override
  String toString() => 'AddFilter { query: $query }';
}
