import 'package:equatable/equatable.dart';

import 'package:flutter_linkarise/models/link.dart';

abstract class FiltersState extends Equatable {
  const FiltersState();

  @override
  List<Object> get props => [];
}

class FiltersLoaded extends FiltersState {
  final List<Link> links;

  final Map query;

  const FiltersLoaded({this.links = const [], this.query = const {}});

  @override
  List<Link> get props => links;

  @override
  String toString() => 'FiltersLoaded { links: $links, query: $query }';
}

class FiltersLoading extends FiltersState {}

class FiltersLoadNotAvailable extends FiltersState {
  final String message;

  const FiltersLoadNotAvailable(this.message);

  @override
  String toString() => 'FiltersLoadNotAvailable { message: $message }';
}
