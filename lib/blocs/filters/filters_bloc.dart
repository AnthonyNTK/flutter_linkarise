import 'dart:async';
import 'package:bloc/bloc.dart';

import 'package:flutter_linkarise/utils/api.dart';
import 'package:flutter_linkarise/models/link.dart';
import 'package:flutter_linkarise/blocs/filters/filters.dart';

class FiltersBloc extends Bloc<FiltersEvent, FiltersState> {
  Map query = Map();

  @override
  FiltersState get initialState => FiltersLoaded();

  @override
  Stream<FiltersState> mapEventToState(FiltersEvent event) async* {
    if (event is AddFilter) {
      yield* _mapAddFilterToState(event);
    } else if (event is ApplyFilter) {
      yield* _mapApplyFilterToState(event);
    }
  }

  Stream<FiltersState> _mapApplyFilterToState(ApplyFilter event) async* {
    query.update(event.key, (v) => event.payload,
        ifAbsent: () => event.payload);
  }

  Stream<FiltersState> _mapAddFilterToState(AddFilter event) async* {
    yield FiltersLoading();

    query.update('tags', (v) => event.query.split(','),
        ifAbsent: () => event.query.split(','));

    try {
      List<Link> links = await API.getSearchLinks(query);

      if (links.length > 0) {
        yield FiltersLoaded(links: links);
      } else {
        yield FiltersLoadNotAvailable('No result found');
      }
      yield FiltersLoaded();
    } catch (err) {
      yield FiltersLoadNotAvailable(err.toString());
    }
  }
}
