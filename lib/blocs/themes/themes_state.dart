import 'package:flutter/material.dart';
import 'package:equatable/equatable.dart';

abstract class ThemesState extends Equatable {
  const ThemesState();

  @override
  List<ThemeData> get props => [];
}

class DarkTheme extends ThemesState {
  final ThemeData themeData;

  const DarkTheme(this.themeData);

  @override
  List<ThemeData> get props => [themeData];

  @override
  String toString() => 'DarkTheme';
}

class LightTheme extends ThemesState {
  final ThemeData themeData;

  const LightTheme(this.themeData);

  @override
  List<ThemeData> get props => [themeData];

  @override
  String toString() => 'LightTheme';
}
