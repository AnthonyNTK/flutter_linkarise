import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:flutter_linkarise/styles/theme.dart';
import 'package:flutter_linkarise/utils/constant.dart';
import 'package:flutter_linkarise/blocs/themes/themes.dart';

class ThemesBloc extends Bloc<ThemesEvent, ThemesState> {
  ThemesBloc();

  @override
  ThemesState get initialState => LightTheme(THEME[AppTheme.Light]);

  @override
  Stream<ThemesState> mapEventToState(ThemesEvent event) async* {
    if (event is ToggleTheme) {
      yield* _mapToggleThemeToState(event);
    } else if (event is InitTheme) {
      yield* _mapInitThemeToState(event);
    }
  }

  Stream<ThemesState> _mapToggleThemeToState(ToggleTheme event) async* {
    SharedPreferences sharedPrefs = await SharedPreferences.getInstance();

    if (state is LightTheme) {
      yield DarkTheme(THEME[AppTheme.Dark]);

      sharedPrefs.setString(THEME_KEY, DARK_THEME_VALUE);
    } else if (state is DarkTheme) {
      yield LightTheme(THEME[AppTheme.Light]);

      sharedPrefs.setString(THEME_KEY, LIGHT_THEME_VALUE);
    }
  }

  Stream<ThemesState> _mapInitThemeToState(InitTheme event) async* {
    if (event.appTheme == AppTheme.Light) {
      yield LightTheme(THEME[event.appTheme]);
    } else if (event.appTheme == AppTheme.Dark) {
      yield DarkTheme(THEME[event.appTheme]);
    }
  }
}
