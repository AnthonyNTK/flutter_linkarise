import 'package:equatable/equatable.dart';

import 'package:flutter_linkarise/styles/theme.dart';

abstract class ThemesEvent extends Equatable {
  const ThemesEvent();

  @override
  List<AppTheme> get props => [];
}

class ToggleTheme extends ThemesEvent {}

class InitTheme extends ThemesEvent {
  final AppTheme appTheme;

  const InitTheme(this.appTheme);

  @override
  String toString() => 'InitTheme { initTheme: $appTheme }';
}
