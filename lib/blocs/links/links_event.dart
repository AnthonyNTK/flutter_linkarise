import 'package:equatable/equatable.dart';
import 'package:flutter_linkarise/models/link.dart';

abstract class LinksEvent extends Equatable {
  const LinksEvent();

  @override
  List<Object> get props => [];
}

class LoadLink extends LinksEvent {}

class AddLink extends LinksEvent {
  final String url;

  const AddLink(this.url);

  @override
  String toString() => 'AddLink { url: $url }';
}

class UpdateLink extends LinksEvent {
  final Link updatedLink;

  const UpdateLink(this.updatedLink);

  @override
  String toString() => 'UpdateLink { updatedLink: $updatedLink }';
}

class DeleteLink extends LinksEvent {
  final String id;

  const DeleteLink(this.id);

  @override
  String toString() => 'DeleteLink { id: $id }';
}
