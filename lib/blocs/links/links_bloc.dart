import 'dart:async';
import 'package:bloc/bloc.dart';

import 'package:flutter_linkarise/utils/api.dart';
import 'package:flutter_linkarise/models/link.dart';
import 'package:flutter_linkarise/utils/cloudstore.dart';
import 'package:flutter_linkarise/blocs/links/links.dart';

class LinksBloc extends Bloc<LinksEvent, LinksState> {
  final LinksState initState;

  LinksBloc(this.initState);

  @override
  LinksState get initialState => initState;

  @override
  Stream<LinksState> mapEventToState(LinksEvent event) async* {
    if (event is LoadLink) {
      yield* _mapLoadLinkToState(event);
    } else if (event is AddLink) {
      yield* _mapAddLinkToState(event);
    } else if (event is UpdateLink) {
      yield* _mapUpdateLinkToState(event);
    } else if (event is DeleteLink) {
      yield* _mapDeleteLinkToState(event);
    }
  }

  Stream<LinksState> _mapLoadLinkToState(LoadLink event) async* {
    if (state is LinksLoading) {
      try {
        final List<Link> savedLinks = await API.fetchLinks();
        yield LinksLoaded(savedLinks);
      } catch (err) {
        yield LinksNotLoaded();
      }
    }
  }

  Stream<LinksState> _mapAddLinkToState(AddLink event) async* {
    final List<Link> updatedLinks = List.from((state as LinksLoaded).links);

    yield LinksLoading();

    bool isLinkExist = await cloudstore.checkLinkExist(event.url);
    if (!isLinkExist) {
      Link newLink = await API.getLinkPreview(event.url);
      updatedLinks.insert(0, newLink);
      cloudstore.addLinkReference(newLink.toJson());

      yield LinksLoaded(updatedLinks);
    } else {
      yield LinksLoadNotAvailable('Links already exist.');
    }
  }

  Stream<LinksState> _mapUpdateLinkToState(UpdateLink event) async* {
    final List<Link> updatedLinks = (state as LinksLoaded).links.map((link) {
      return link.id == event.updatedLink.id ? event.updatedLink : link;
    }).toList();
    cloudstore.updateLinkReference(event.updatedLink.id, event.updatedLink.tags);
    yield LinksLoaded(updatedLinks);
  }

  Stream<LinksState> _mapDeleteLinkToState(DeleteLink event) async* {
    final updatedLinks = (state as LinksLoaded)
        .links
        .where((link) => link.id != event.id)
        .toList();
    cloudstore.deleteLinkReference(event.id);
    yield LinksLoaded(updatedLinks);
  }
}
