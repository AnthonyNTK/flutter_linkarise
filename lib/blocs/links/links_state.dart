import 'package:equatable/equatable.dart';

import 'package:flutter_linkarise/models/link.dart';

abstract class LinksState extends Equatable {
  const LinksState();

  @override
  List<Object> get props => [];
}

class LinksLoaded extends LinksState {
  final List<Link> links;

  const LinksLoaded([this.links = const []]);

  @override
  List<Link> get props => links;

  @override
  String toString() => 'LinksLoaded { links: $links }';
}

class LinksLoading extends LinksState {}

class LinksNotLoaded extends LinksState {}

class LinksLoadNotAvailable extends LinksState {
  final String message;

  const LinksLoadNotAvailable(this.message);

  @override
  String toString() => 'LinksLoadNotAvailable { message: $message }';
}
