import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_linkarise/blocs/filters/filters.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';

import 'package:flutter_linkarise/utils/api.dart';
import 'package:flutter_linkarise/blocs/blocs.dart';
import 'package:flutter_linkarise/styles/theme.dart';
import 'package:flutter_linkarise/utils/constant.dart';
import 'package:flutter_linkarise/screens/screens.dart';
import 'package:flutter_linkarise/screens/widgets/bottom_navy_bar.dart';

void main() {
  BlocSupervisor.delegate = SimpleBlocDelegate();
  runApp(MultiBlocProvider(
    providers: [
      BlocProvider<LinksBloc>(
        builder: (context) => LinksBloc(LinksLoading()),
      ),
      BlocProvider<ThemesBloc>(
        builder: (context) => ThemesBloc(),
      ),
      BlocProvider<FiltersBloc>(
        builder: (context) => FiltersBloc(),
      )
    ],
    child: LinksApp(),
  ));
}

class LinksApp extends StatefulWidget {
  @override
  _LinksAppState createState() => _LinksAppState();
}

class _LinksAppState extends State<LinksApp> {
  PageController _pageController;

  int currentIndex = 0;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();

    loadPrefs();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  void navigateToPage(int index) {
    if (index != currentIndex)
      setState(() {
        currentIndex = index;
        _pageController.animateToPage(
          index,
          duration: const Duration(milliseconds: 400),
          curve: Curves.easeInOut,
        );
      });
  }

  void loadPrefs() async {
    final SharedPreferences sharedPrefs = await SharedPreferences.getInstance();
    String theme = sharedPrefs.getString(THEME_KEY) ?? LIGHT_THEME_VALUE;

    AppTheme appTheme =
        theme == DARK_THEME_VALUE ? AppTheme.Dark : AppTheme.Light;
    BlocProvider.of<ThemesBloc>(context).add(InitTheme(appTheme));

    final RemoteConfig remoteConfig = await RemoteConfig.instance;
    await remoteConfig.fetch(expiration: const Duration(seconds: 0));
    await remoteConfig.activateFetched();
    API.apiKey = remoteConfig.getString(PREVIEW_API_KEY);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ThemesBloc, ThemesState>(
        builder: (BuildContext context, ThemesState state) {
      return MaterialApp(
        theme: state.props.first,
        home: SafeArea(
          child: Scaffold(
              body: PageView(
                physics: NeverScrollableScrollPhysics(),
                controller: _pageController,
                children: <Widget>[
                  HomeScreen(
                    initData: () =>
                        BlocProvider.of<LinksBloc>(context).add(LoadLink()),
                  ),
                  SearchScreen(),
                  ChartScreen(),
                  SettingScreen(
                    toggleTheme: () =>
                        BlocProvider.of<ThemesBloc>(context).add(ToggleTheme()),
                  )
                ],
              ),
              bottomNavigationBar: BottomNavyBar(
                  selectedIndex: currentIndex,
                  showElevation: true,
                  onItemSelected: navigateToPage,
                  backgroundColor: state.props.first.primaryColor)),
        ),
      );
    });
  }
}
