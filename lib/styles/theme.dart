import 'package:flutter/material.dart';

enum AppTheme {
  Light,
  Dark,
}

final THEME = {
  AppTheme.Light: ThemeData(
      brightness: Brightness.light,
      primaryColor: Colors.white,
      snackBarTheme: SnackBarThemeData(
          backgroundColor: Colors.white,
          contentTextStyle: TextStyle(color: Colors.white))),
  AppTheme.Dark: ThemeData(
      brightness: Brightness.dark,
      primaryColor: Colors.black,
      snackBarTheme: SnackBarThemeData(
          backgroundColor: Colors.black,
          contentTextStyle: TextStyle(color: Colors.white))),
};
