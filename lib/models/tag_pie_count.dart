class TagPieCount {
  final String tag;
  final int count;

  TagPieCount({this.tag, this.count});
}