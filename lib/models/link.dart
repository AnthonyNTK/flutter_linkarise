import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

import 'package:flutter_linkarise/utils/uuid.dart';

@immutable
class Link extends Equatable {
  final String id;
  final String title;
  final String description;
  final String image;
  final String url;
  final DateTime createdAt;
  final List<String> tags;
  final bool isRecent;

  Link(
      {this.title,
      String description = '',
      String image = '',
      String id,
      @required this.url,
      DateTime createdAt,
      List<String> tags,
      bool isRecent})
      : this.id = id ?? uuid.v4(),
        this.description = description ?? '',
        this.image = image ?? '',
        this.createdAt = createdAt ?? DateTime.now(),
        this.tags = tags ?? List(),
        this.isRecent = isRecent ?? true;

  Link copyWith(
      {String title,
      String description,
      String image,
      String url,
      String id,
      DateTime createdAt,
      List<String> tags,
      bool isRecent}) {
    return Link(
        title: title ?? this.title,
        description: description ?? this.description,
        id: id ?? this.id,
        image: image ?? this.image,
        url: url ?? this.url,
        createdAt: createdAt ?? this.createdAt,
        tags: tags ?? this.tags,
        isRecent: isRecent ?? this.isRecent);
  }

  Link.fromJson(Map<String, dynamic> json)
      : id = json['id'] ?? uuid.v4(),
        title = json['title'],
        description = json['description'],
        image = json['image'],
        url = json['url'],
        createdAt = json['createdAt'] == null
            ? DateTime.now()
            : DateTime.parse(json['createdAt']),
        tags = json['tags'] == null ? List() : List.from(json['tags']),
        isRecent = json['createdAt'] == null
            ? true
            : _isRecent(DateTime.now(), DateTime.parse(json['createdAt']))
                ? true
                : false;

  Map<String, dynamic> toJson() => {
        'id': id,
        'title': title,
        'description': description,
        'image': image,
        'url': url,
        'createdAt': createdAt.toIso8601String(),
        'tags': tags
      };

  @override
  String toString() {
    return 'Link { title: $title, description: $description, image: $image, id: $id, url: $url, isRecent: $isRecent }';
  }

  @override
  List<Object> get props =>
      [title, description, image, id, url, createdAt, tags, isRecent];

  static bool _isRecent(DateTime date1, DateTime date2) {
    return date1.difference(date2).inDays <= 2;
  }
}
