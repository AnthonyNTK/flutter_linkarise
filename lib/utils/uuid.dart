import 'package:uuid/uuid.dart';

class UUID {
  static final UUID _singleton = UUID._internal();

  final uuid = new Uuid();

  factory UUID() {
    return _singleton;
  }

  UUID._internal();

  v1() {
    return uuid.v1();
  }

  v4() {
    return uuid.v4();
  }

  v5(String namespace, String name) {
    return uuid.v5(namespace, name);
  }
}

final uuid = UUID();