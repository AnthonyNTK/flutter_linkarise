import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:flutter_linkarise/models/link.dart';
import 'package:flutter_linkarise/utils/constant.dart';
import 'package:flutter_linkarise/utils/cloudstore.dart';

class API {
  static String apiKey = '';

  static Future<List<Link>> fetchLinks() async {
    List<Link> links = List();

    QuerySnapshot snapshot = await cloudstore.getLinkReference(100);
    snapshot.documents
        .forEach((document) => links.add(Link.fromJson(document.data)));

    return links;
  }

  static Future<Link> getLinkPreview(String url) async {
    final response = await http.get('$PREVIEW_AP_ENDPOINT?key=$apiKey&q=$url');

    if (response.statusCode == 200) {
      return Link.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load link preview');
    }
  }

  static Future<Map> getUserStatistic() async {
    final response = await http.get(CLOUD_USER_STATISTIC_FUNCTION_ENDPOINT);
    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception('Failed to load user statistic');
    }
  }

  static Future<Map> getDataStatistic() async {
    final response = await http.get(CLOUD_DATA_STATISTIC_FUNCTION_ENDPOINT);
    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception('Failed to load data statistic');
    }
  }

  static Future<List<Link>> getSearchLinks(Map query) async {
    final response = await http.post(CLOUD_SEARCH_LINK_FUNCTION_ENDPOINT,
        headers: {'Content-Type': 'application/json'},
        body: json.encode(query));
    if (response.statusCode == 200) {
      List<Link> links = List();

      Map data = json.decode(response.body);
      data['links'].forEach((link) => links.add(Link.fromJson(link)));

      return links;
    } else {
      throw Exception('Failed to search link');
    }
  }
}
