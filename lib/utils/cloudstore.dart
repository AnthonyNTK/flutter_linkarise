import 'package:cloud_firestore/cloud_firestore.dart';

class CLOUDSTORE {
  static final CLOUDSTORE _singleton = CLOUDSTORE._internal();

  final Firestore firestore = Firestore.instance;

  factory CLOUDSTORE() {
    return _singleton;
  }

  CLOUDSTORE._internal();

  CollectionReference linkCollection() {
    return firestore.collection('link');
  }

  void addLinkReference(Map data) {
    linkCollection().document(data['id']).setData(data);
  }

  void updateLinkReference(String id, List<String> tags) {
    DocumentReference documentReference = linkCollection().document(id);

    firestore.runTransaction((transaction) {
      return transaction.update(documentReference, { 'tags': tags });
    }).then((newLink) {

    }).catchError((error) {

    });
  }

  void deleteLinkReference(String id) {
    DocumentReference documentReference = linkCollection().document(id);

    firestore.runTransaction((transaction) {
      return transaction.delete(documentReference);
    }).then((newLink) {

    }).catchError((error) {

    });
  }

  Future<QuerySnapshot> getLinkReference(int limit) {
    return linkCollection().limit(limit).orderBy('createdAt', descending: true).getDocuments();
  }

  Future<bool> checkLinkExist(String url) async {
    QuerySnapshot snapshot =
        await linkCollection().where('url', isEqualTo: url.replaceAll(' ', '')).getDocuments();
    return snapshot.documents.length > 0;
  }
}

final cloudstore = CLOUDSTORE();
