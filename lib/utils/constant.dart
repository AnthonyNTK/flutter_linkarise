const THEME_KEY = 'THEME_KEY';
const DARK_THEME_VALUE = 'DARK_THEME';
const LIGHT_THEME_VALUE = 'LIGHT_THEME';

const PREVIEW_AP_ENDPOINT = 'http://api.linkpreview.net/';
const PREVIEW_API_KEY = 'link_preview_api_key';

const CLOUD_USER_STATISTIC_FUNCTION_ENDPOINT = 'https://us-central1-flutter-linkarise.cloudfunctions.net/statisticCount';
const CLOUD_DATA_STATISTIC_FUNCTION_ENDPOINT = 'https://us-central1-flutter-linkarise.cloudfunctions.net/dataCount';
const CLOUD_SEARCH_LINK_FUNCTION_ENDPOINT = 'https://us-central1-flutter-linkarise.cloudfunctions.net/searchLink';

enum BLOC_TYPE {
  FILTER, LINK
}

enum CATEGORY {
  TAG, CONTENT, DATE
}
