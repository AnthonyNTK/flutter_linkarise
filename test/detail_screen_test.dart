import 'dart:io';

import 'network_image_mock.dart';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:flutter_linkarise/models/link.dart';
import 'package:flutter_linkarise/screens/screens.dart';
import 'package:flutter_linkarise/screens/widgets/widgets.dart';

void main() {
  group('Detail Screen', () {
    testWidgets('renders correctly', (WidgetTester tester) async {
      HttpOverrides.runZoned(() async {
        final List<String> tags = ['abc', 'cde'];
        Link link = Link(
            url: 'https://www.exmaple.com',
            title: 'Test Title',
            description: 'Test Description',
            image: 'https://via.placeholder.com/150.png',
            tags: tags);

        await tester.pumpWidget(MaterialApp(
          home: DetailScreen(link),
        ));

        expect(find.byType(DetailScreen), findsOneWidget);
        expect(find.byType(Hero), findsOneWidget);
        expect(find.byType(TagList), findsOneWidget);

        final tagWidgets = find.byType(Chip);
        expect(tagWidgets, findsNWidgets(2));
        final tagWidgetsText =
            find.descendant(of: tagWidgets, matching: find.byType(Text));
        tagWidgetsText.evaluate().forEach((tagWidgetText) {
          final tagText = tagWidgetText.widget as Text;
          expect(true, tags.contains(tagText.data));
        });

        final linkTitle = find.byType(SectionTitle);
        final linkDescription = find.byType(LinkDescription);
        expect(linkTitle, findsOneWidget);
        expect(linkDescription, findsOneWidget);

        final linkTitleText = find
            .descendant(of: linkTitle, matching: find.byType(Text))
            .evaluate()
            .single
            .widget as Text;
        final linkDescriptionText = find
            .descendant(of: linkDescription, matching: find.byType(Text))
            .evaluate()
            .single
            .widget as Text;

        expect(linkTitleText.data, link.title);
        expect(linkDescriptionText.data, link.description);
      }, createHttpClient: MockHttpClientFactory.createHttpClient);
    });

    testWidgets('renders correctly with dialog', (WidgetTester tester) async {
      HttpOverrides.runZoned(() async {
        final List<String> tags = ['abc', 'cde'];
        Link link = Link(
            url: 'https://www.exmaple.com',
            title: 'Test Title',
            description: 'Test Description',
            image: 'https://via.placeholder.com/150.png',
            tags: tags);

        await tester.pumpWidget(MaterialApp(
          home: DetailScreen(link),
        ));

        final iconButtonWidgets = find.byType(IconButton);
        expect(iconButtonWidgets, findsNWidgets(2));

        await tester.tap(find.byIcon(Icons.add));
        await tester.pumpAndSettle();
        expect(find.byType(AlertDialog), findsOneWidget);
        expect(find.byType(TagList), findsOneWidget);
        expect(find.byType(FlatButton), findsNWidgets(2));

        await tester.enterText(find.byType(TextField), 'tag');
        await tester.tap(find.byIcon(Icons.check));
        await tester.pumpAndSettle();
        expect(find.byType(Chip), findsNWidgets(3));

        await tester.tap(find.text('CANCEL'));
        await tester.pumpAndSettle();
        expect(find.byType(AlertDialog), findsNothing);
      }, createHttpClient: MockHttpClientFactory.createHttpClient);
    });
  });
}
