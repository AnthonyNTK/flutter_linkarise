import './home_screen_test.dart' as homeScreen;
import './detail_screen_test.dart' as detailScreen;

main() {
  homeScreen.main();
  detailScreen.main();
}