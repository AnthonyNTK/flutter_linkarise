import 'dart:io';

import 'network_image_mock.dart';

import 'package:flutter/material.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_linkarise/main.dart';
import 'package:flutter_linkarise/blocs/blocs.dart';
import 'package:flutter_linkarise/models/link.dart';
import 'package:flutter_linkarise/styles/theme.dart';
import 'package:flutter_linkarise/screens/screens.dart';
import 'package:flutter_linkarise/screens/widgets/widgets.dart';

class MockLinksBloc extends Mock implements LinksBloc {}

class MockThemesBloc extends Mock implements ThemesBloc {}

void main() {
  group('Home Screen', () {
    ThemesBloc themesBloc;

    setUp(() {
      themesBloc = MockThemesBloc();
    });

    testWidgets('renders correctly', (WidgetTester tester) async {
      when(themesBloc.state)
          .thenAnswer((_) => LightTheme(THEME[AppTheme.Light]));
      await tester.pumpWidget(MultiBlocProvider(
        providers: [
          BlocProvider<LinksBloc>(
            builder: (context) => LinksBloc(LinksLoading()),
          ),
          BlocProvider<ThemesBloc>(
            builder: (context) => ThemesBloc(),
          )
        ],
        child: LinksApp(),
      ));

      expect(find.byType(HomeScreen), findsOneWidget);
      expect(find.byType(BottomNavyBar), findsOneWidget);
      expect(find.byType(AnimatedContainer), findsNWidgets(4));
      expect(find.byType(CircularProgressIndicator), findsOneWidget);
    });

    testWidgets('renders correctly with preview card',
        (WidgetTester tester) async {
      HttpOverrides.runZoned(() async {
        List<Link> links = [
          Link(
              url: 'https://www.exmaple.com/1',
              title: 'Test Title 1',
              description: 'Test Description',
              image: 'https://via.placeholder.com/150.png',
              isRecent: true),
          Link(
              url: 'https://www.exmaple.com/2',
              title: 'Test Title 2',
              description: 'Test Description',
              image: 'https://via.placeholder.com/150.png'),
          Link(
              url: 'https://www.exmaple.com/3',
              title: 'Test Title 3',
              description: 'Test Description',
              image: 'https://via.placeholder.com/150.png')
        ];

        when(themesBloc.state)
            .thenAnswer((_) => LightTheme(THEME[AppTheme.Light]));
        await tester.pumpWidget(MultiBlocProvider(providers: [
          BlocProvider<LinksBloc>(
            builder: (context) => LinksBloc(LinksLoaded(links)),
          ),
          BlocProvider<ThemesBloc>(
            builder: (context) => ThemesBloc(),
          )
        ], child: LinksApp()));

        expect(find.byType(HomeScreen), findsOneWidget);
        expect(find.byType(BottomNavyBar), findsOneWidget);
        expect(find.byType(AnimatedContainer), findsNWidgets(4));
        expect(find.byType(CircularProgressIndicator), findsNothing);
        expect(find.byType(PreviewCard), findsNWidgets(3));
        expect(find.byType(CardButton), findsNWidgets(12));
        expect(find.byType(LinkDescription), findsNWidgets(3));
        expect(find.byType(LinkContent), findsNWidgets(3));
      }, createHttpClient: MockHttpClientFactory.createHttpClient);
    });

    testWidgets('navigate to detail screen correctly', (WidgetTester tester) async {
      List<Link> links = [
        Link(
            url: 'https://www.exmaple.com/1',
            title: 'Test Title 1',
            description: 'Test Description',
            image: 'https://via.placeholder.com/150.png',
            isRecent: true),
        Link(
            url: 'https://www.exmaple.com/2',
            title: 'Test Title 2',
            description: 'Test Description',
            image: 'https://via.placeholder.com/150.png'),
        Link(
            url: 'https://www.exmaple.com/3',
            title: 'Test Title 3',
            description: 'Test Description',
            image: 'https://via.placeholder.com/150.png')
      ];

      when(themesBloc.state)
          .thenAnswer((_) => LightTheme(THEME[AppTheme.Light]));
      await tester.pumpWidget(MultiBlocProvider(providers: [
        BlocProvider<LinksBloc>(
          builder: (context) => LinksBloc(LinksLoaded(links)),
        ),
        BlocProvider<ThemesBloc>(
          builder: (context) => ThemesBloc(),
        )
      ], child: LinksApp()));

      await tester.tap(find.text(links[0].title));
      await tester.pumpAndSettle();

      expect(find.byType(DetailScreen), findsOneWidget);
    });
  });
}
