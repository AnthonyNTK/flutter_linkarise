const admin = require('firebase-admin')
const firestore = admin.firestore()

exports.handler = (req, res) => {
  if (req.method === 'PUT' || req.method === 'DELETE') {
    return res.status(403).send('Forbidden!')
  }

  firestore.collection('link').get().then(snapshot => {
    const totalLength = snapshot.docs.length

    let totalTitleChar = 0
    let totalDescriptionChar = 0
    let averageTitleChar = 0
    let averageDescriptionChar = 0

    snapshot.docs.forEach(docRef => {
      const link = docRef.data()
      totalTitleChar += link.title.length
      totalDescriptionChar += link.description.length
    })

    averageTitleChar = totalTitleChar / totalLength
    averageDescriptionChar = totalDescriptionChar / totalLength

    return res.status(200).send({totalTitleChar, totalDescriptionChar, averageTitleChar, averageDescriptionChar})
  }).catch(err => {
    console.log('Error getting documents', err)
    return res.status(500).send('Error!')
  })
}
