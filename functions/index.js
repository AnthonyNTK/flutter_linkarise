const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config.firebase);

const statisticCountModule = require('./statisticCount');
const dataCountModule = require('./dataCount');
const searchLinkModule = require('./searchLink');

exports.statisticCount = functions.https.onRequest(statisticCountModule.handler);
exports.dataCount = functions.https.onRequest(dataCountModule.handler);
exports.searchLink = functions.https.onRequest(searchLinkModule.handler);
