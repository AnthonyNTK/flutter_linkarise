const admin = require('firebase-admin')
const firestore = admin.firestore()

exports.handler = (req, res) => {
  if (req.method === 'PUT' || req.method === 'DELETE') {
    return res.status(403).send('Forbidden!')
  }

  firestore.collection('link').get().then(snapshot => {
    const tagCount = {}
    const linkCount = {}

    const totalLink = snapshot.docs.length
    let totalTag = 0
    let maxTag = 0

    let averageTag = 0
    let averageLink = 0

    snapshot.docs.forEach(docRef => {
      const link = docRef.data()
      totalTag += link.tags.length
      if (maxTag < link.tags.length) maxTag = link.tags.length
      link.tags.forEach((tag) => {
        if (tagCount[tag]) tagCount[tag] += 1
        else tagCount[tag] = 1
      })

      const createdAt = new Date(link.createdAt)
      const date = createdAt.getFullYear() + '-' + (createdAt.getMonth() + 1) + '-' + createdAt.getDate();
      if (linkCount[date]) linkCount[date] += 1
      else linkCount[date] = 1
    })

    averageLink = totalLink / Object.keys(linkCount).length
    averageTag = totalTag / totalLink

    return res.status(200).send({tagCount, linkCount, totalTag, totalLink, maxTag, averageLink, averageTag})
  }).catch(err => {
    console.log('Error getting documents', err)
    return res.status(500).send('Error!')
  })
}
