const admin = require('firebase-admin')
const firestore = admin.firestore()

exports.handler = (req, res) => {
  if (req.method === 'PUT' || req.method === 'DELETE') {
    return res.status(403).send('Forbidden!')
  }

  const query = req.body
  console.log(query)

  const lowerDateLimit = query.date || '2018-01-01'

  if (query.id === undefined) {
    firestore.collection('link')
      .orderBy('createdAt')
      .where('tags', 'array-contains-any', query.tags)
      .where('createdAt', '>=', lowerDateLimit)
      .limit(20)
      .get()
      .then(snapshot => {
        const links = []

        snapshot.docs.forEach(docRef => links.push(docRef.data()))
        return res.status(200).send({links})
      })
      .catch(err => {
        console.log('Error getting documents', err)
        return res.status(500).send('Error!')
      })
  } else {
    const docRef = firestore.collection('link').doc(query.id)
    docRef.get().then(snapshot => {
      const paginatedRef = firestore.collection('link')
                            .orderBy('createdAt')
                            .where('tags', 'array-contains-any', query.tags)
                            .startAfter(snapshot)
      return paginatedRef.limit(20).get()
    }).then(snapshot => {
      const links = []

      snapshot.docs.forEach(docRef => links.push(docRef.data()))
      return res.status(200).send({links})
    }).catch(err => {
      console.log('Error getting documents', err)
      return res.status(500).send('Error!')
    })
  }
}
